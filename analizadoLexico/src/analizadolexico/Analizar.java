
package analizadolexico;

public class Analizar {
    
    static Lenguaje lang = new Lenguaje();
    static Impresion impres = new Impresion();
    
    public static void analizarString(String string_in) {
        
        String cont = "";
        string_in += " ";
        
        for(int i=0; i<string_in.length(); i++){
            char a = string_in.charAt(i);
            
            if(a != ' '){
                cont += a;
            }else{
                analizar(cont);
                cont = "";
            }
        }
        impres.lex();
    }
    
    private static void analizar(String string_in) {
        int acum = 0;
        
        for(int i=0; i<lang.palabras_reservadas.length; i++){
            if(string_in.equals(lang.palabras_reservadas[i])){
                acum++;
                impres.printFunct(1, string_in);
                break;
            }
        }
        
        for(int i=0; i<lang.operadores_aritmeticos.length; i++){
            if(string_in.equals(lang.operadores_aritmeticos[i])){
                acum++;
                impres.printFunct(2, string_in);
                break;
            }
        }
        
        for(int i=0; i<lang.operadores_logicos.length; i++){
            if(string_in.equals(lang.operadores_logicos[i])){
                acum++;
                impres.printFunct(3, string_in);
                break;
            }
        }
        
        for(int i=0; i<lang.operadores_relacionales.length; i++){
            if(string_in.equals(lang.operadores_relacionales[i])){
                acum++;
                impres.printFunct(4, string_in);
                break;
            }
        }
        
        for(int i=0; i<lang.simbolos.length; i++){
            if(string_in.equals(lang.simbolos[i])){
                acum++;
                impres.printFunct(5, string_in);
                break;
            }
        }
        
        for(int i=0; i<lang.operadores_boleanos.length; i++){
            if(string_in.equals(lang.operadores_boleanos[i])){
                acum++;
                impres.printFunct(6, string_in);
                break;
            }
        }
        
        if(acum==0){
            try{
                float aux = Float.parseFloat(string_in);
                impres.printFunct(8, string_in);
            }catch(NumberFormatException e){
                if(string_in.charAt(0) == '\"' && 
                    string_in.charAt(string_in.length()-1) == '\"'){
                    
                    string_in = string_in.replaceAll("\"", "");
                    string_in = "\"" + string_in + "\"";
                    impres.printFunct(7, string_in);
                    
                }else if(string_in.charAt(0) == '\'' && 
                    string_in.charAt(string_in.length()-1) == '\''){
                    
                    string_in = string_in.replaceAll("\'", "");
                    string_in = "\'" + string_in + "\'";
                    impres.printFunct(7, string_in);
                    
                }else if(Character.isAlphabetic(string_in.charAt(0)) &&
                         Character.isAlphabetic(string_in.charAt(string_in.length()-1)) ||
                         Character.isDigit(string_in.charAt(string_in.length()-1)) ||
                         string_in.charAt(0) == '_'){
                    
                    impres.printFunct(9, string_in);
                    
                }else if(!impres.balance(string_in)) {
                    impres.errorFunct(2, string_in);
                }else{
                    impres.errorFunct(1, string_in);
                }
            }
        }
    }

    
}
