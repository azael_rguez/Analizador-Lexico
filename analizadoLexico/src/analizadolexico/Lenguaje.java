
package analizadolexico;

public class Lenguaje {
    String palabras_reservadas[] = {"abstract", "assert", "boolean", "break",
                                    "byte", "case", "catch", "char", "class", 
                                    "const", "continue", "default", "do", 
                                    "double", "else", "enum", "extends", "final",
                                    "finally", "float", "for", "goto", "if", 
                                    "implements", "instanceof", "int", "interface",
                                    "long", "native", "new", "package", "private",
                                    "protected", "public", "return", "short",
                                    "static", "strictfp", "super", "switch", 
                                    "synchronized", "this", "throw", "throws", 
                                    "transient", "try", "void", "volatile", 
                                    "while"};
    
    String operadores_aritmeticos[] = {"=", "+", "-", "*", "/", "%", "+=", "-=", 
                                       "*=", "/="};
    
    String operadores_logicos[] = {"&&", "||", "!", "null"};
    
    String operadores_relacionales[] = { ">=", "<=", "==", "!=", ">", "<", "?"};
    
    String simbolos[] = {"{", "}", "[", "]", "(", ")", ",", ".", ":", ";", "#", 
                        "$", "@"};
    
    String operadores_boleanos[] = {"true", "false"};
    
}
