
package analizadolexico;

import java.util.Stack;

public class Impresion {
    
    static Lexico lex = new Lexico();
    
    void lex() {
        lex.analize();
    }
    
    public static void printFunct(int i, String string_in) {
        String opc = "";
        switch(i){
            case 1:
                opc = "Palabra reservada";
                break;
            case 2:
                opc = "Operador aritmetico";
                break;
            case 3:
                opc = "Operador logico";
                break;
            case 4:
                opc = "Operador relacional";
                break;
            case 5:
                opc = "Simbolo";
                break;
            case 6:
                opc = "Operador booleano";
                break;
            case 7:
                opc = "Conjunto de caracteres" + " (" + 
                        string_in.length() + " caracteres)";
                break;
            case 8:
                opc = "Conjunto de numeros"+ " (" + 
                        string_in.length() + " digitos)";
                break;
            case 9:
                opc = "Variable";
                break;
        }
        System.out.print(string_in + "\t:\t" + opc + "\n");
        lex.expression.add(string_in);
    }
    
    public static void errorFunct(int i, String string_in){
        String opc = "";
        switch(i){
            case 1:
                opc = "Error de identificador";
                break;
            case 2:
                opc = "Error de parentesis";
                break;
        }
        System.out.print(string_in + "\t:\t" + opc + "\n");
    }

    boolean balance(String string_in) {
        Stack<Character> balanceo = new Stack<Character>();
        for(int i = 0; i < string_in.length(); i++){
            char c = string_in.charAt(i);
            if(c == '[' || c == '(' || c == '{' ){
                balanceo.push(c);
            } else if(c == ']') {
                if(balanceo.isEmpty() || balanceo.pop() != '[') {
                    return false;
                }
            } else if(c == ')') {
                if(balanceo.isEmpty() || balanceo.pop() != '(') {
                    return false;
                }           
            } else if(c == '}') {
                if(balanceo.isEmpty() || balanceo.pop() != '{') {
                    return false;
                }
            }
        }
        return balanceo.isEmpty();
    }
}
