
package analizadolexico;

import java.util.LinkedList;
import java.util.Queue;

public class Lexico {
    
    static Queue<String> expression = new LinkedList<String>();
    
    void analize(){
        System.out.print(expression.remove()  + " -> ");
        while(!expression.isEmpty()){
            System.out.print(expression.remove()  + " ");
        }
    }
    
}
