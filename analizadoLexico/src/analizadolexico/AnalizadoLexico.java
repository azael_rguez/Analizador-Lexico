/**
 *
 * @author azael rodriguez
 */

package analizadolexico;

import java.util.Scanner;

public class AnalizadoLexico {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String string_in = "";
        
        do{
            System.out.println("\nEntrada: ");
            string_in = scan.nextLine();
            Analizar.analizarString(string_in.replaceAll("\\s+", " ").trim());
        }while(true);
        
    }
    
}
